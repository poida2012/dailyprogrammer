Daily Programmer
================

This holds code that I''ve written for the Reddit dailyprogrammer challenges.

See http://www.reddit.com/r/dailyprogrammer.


Wordlists
---------

Wordlists are from the following sources:

- wordlists/UKACD17.TXT
    - The UK Advanced Cryptics Dictionary v1.7
    - Copyright (c) J Ross Beresford 1993-2000. All Rights Reserved.
    - URL: http://cfaj.freeshell.org/wordfinder/UKACD17.shtml
    - License: wordlists/UKACD17-README.TXT.
    - dos2unix''d and converted to UTF-8: wordlists/UKACD17.UTF8.TXT
