module Roman where

import Text.ParserCombinators.Parsec

data Numeral = I | V | X | L | C | D | M deriving (Read, Show)
type RomNum = [Numeral]
data RomAdv = Bracketed RomAdv RomNum | Simple Numeral RomNum deriving (Show)

fromRoman :: Numeral -> Int
fromRoman I = 1
fromRoman V = 5
fromRoman X = 10
fromRoman L = 50
fromRoman C = 100
fromRoman D = 500
fromRoman M = 1000

fromRomans :: RomNum -> Int
fromRomans []       = 0
fromRomans (x:[])   = fromRoman x
fromRomans (x:y:xs) = if a >= b
                      then a + fromRomans (y:xs)
                      else (b - a) + fromRomans xs
  where a = fromRoman x
        b = fromRoman y

eval :: RomAdv -> Int
eval (Simple x y) = fromRomans (x:y)
eval (Bracketed x y) = (1000 * (eval x)) + (fromRomans y)

romNumbers :: GenParser Char st [RomAdv]
romNumbers = do
  numbers <- sepBy (romAdv) (char '\n')
  eof
  return numbers

romAdv :: GenParser Char st RomAdv
romAdv = try simple <|> complex

complex :: GenParser Char st RomAdv
complex =
  do multed <- between (char '(') (char ')') romAdv
     remainder <- romNum
     return (Bracketed multed remainder)

simple :: GenParser Char st RomAdv
simple = do
  number1 <- numeral
  number <- romNum
  return (Simple number1 number)

romNum :: GenParser Char st [Numeral]
romNum = many numeral

numeral :: GenParser Char st Numeral
numeral = do
  c <- char 'I' <|> char 'V' <|> char 'X' <|> char 'L' <|> char 'C' <|> char 'D' <|> char 'M'
  return $ read [c]

main = do
  c <- getContents
  case parse romNumbers "(stdin)" c of
    (Left error) -> print error
    (Right xs) -> mapM_ (print) (map (show . eval) xs)
