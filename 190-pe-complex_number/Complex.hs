module Complex where

data Complex = Complex { real :: Float, imaginary :: Float }

modulus   (Complex a b) = sqrt (a*a + b*b)
conjugate (Complex a b) = Complex a (-b)

add      (Complex a b) (Complex c d) = (Complex (a + c) (b + d))
subtract (Complex a b) (Complex c d) = (Complex (a - c) (b - d))
multiply (Complex a b) (Complex c d) = (Complex (a * c - b * d) (a * d + b * c))
divide   (Complex a b) (Complex c d) = (Complex ((a * c + b * d) / ad2) ((b * c - a * d) / ad2))
  where ad2 = c * c + d * d

instance Show Complex where
  show (Complex 0 0) = "0"
  show (Complex 0 b) = (show b) ++ "i"
  show (Complex a 0) = show a
  show (Complex a b) = (show a) ++ plus ++ (show b) ++ "i"
    where plus = if b > 0 then "+" else ""
