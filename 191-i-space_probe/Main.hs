module Main where

import Control.Monad.State
import System.Random
import Control.Monad.Loops
import Data.Maybe

data SpaceJunk = A | G | E | S deriving Show
type SpaceMap = [[SpaceJunk]]
type Position = (Int, Int)

getPos :: SpaceMap -> Position -> SpaceJunk
getPos sm (x,y) = sm !! x !! y

setPos :: SpaceMap -> Position -> SpaceJunk -> SpaceMap
setPos sm (x,y) j = xa ++ [ya ++ [j] ++ yd] ++ xd
  where (xa, xb) = splitAt x sm
        ([xc], xd) = splitAt 1 xb
        (ya, yb) = splitAt y xc
        (yc, yd) = splitAt 1 yb

asteroidRatio :: Float
asteroidRatio = 0.3

gravityWellRatio :: Float
gravityWellRatio = 0.1

freeSpaceRatio :: Float
freeSpaceRatio = 0.6

empty :: Int -> SpaceMap
empty n = replicate n (replicate n E)

randomIdx :: Int -> IO Int
randomIdx n = do
  g <- getStdGen
  let (i,g') = randomR (0,n) g
  setStdGen g'
  return i

randomPos :: Int -> IO Position
randomPos n
  = do g <- getStdGen
       let (x,g') = randomR (0,(n-1)) g
       let (y,g'') = randomR (0,(n-1)) g'
       setStdGen g''
       return (x,y)

randomiseJunk :: SpaceMap -> SpaceJunk -> IO (Maybe SpaceMap)
randomiseJunk sm j
  = do let n = length sm
       pos <- randomPos n
       return $ case (getPos sm pos) of
         E -> Just (setPos sm pos j)
         _ -> Nothing

randomiseJunks :: Int -> SpaceJunk -> StateT (SpaceMap,Int,Int) IO SpaceMap
randomiseJunks x j
  = do whileM_
        (do (m,g,b) <- get
            return $ if (b == x)
                     then (error "unable to generate terrain")
                     else (if (g == x)
                           then False
                           else True))
        (do (m,g,b) <- get
            m' <- lift $ randomiseJunk m j
            case m' of
              (Just m'') -> do put (m'',g+1,b)
                               return ()
              Nothing -> do put (m,g,b+1)
                            return ())
       (m,g,b) <- get
       return m

generateMap :: Int -> IO SpaceMap
generateMap n
  = do m <- evalStateT (randomiseJunks numAsteroids A) ((empty n),0,0)
       evalStateT (randomiseJunks numGravityWells G) (m,0,0)
  where numAsteroids = floor $ area * asteroidRatio
        numGravityWells = floor $ area * gravityWellRatio
        area = fromIntegral $ n * n

vacantPos :: SpaceMap -> [Position]
vacantPos sm
  = catMaybes $ do x <- [0..(n-1)]
                   y <- [0..(n-1)]
                   return $ case getPos sm (x,y) of
                     E -> Just (x,y)
                     _ -> Nothing
  where n = length(sm)

applyPos :: SpaceMap -> [Position] -> SpaceJunk -> SpaceMap
applyPos sm ps j = foldr (\p -> \sm' -> setPos sm' p j) sm ps

main :: IO ()
main = return ()
