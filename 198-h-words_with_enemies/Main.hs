module Main where

import WordCannon (Winner (..), smashword)
import Data.Char (toLower)
import Data.List (isInfixOf, sort)

wordlistfile = "../wordlists/UKACD17.UTF8.TXT"

turns = 5

data WordPlayer = IOPlayer | AIPlayer

nobadchars x = not (or [isInfixOf " " x, isInfixOf "-" x])

getwords :: IO [String]
getwords = do
  ws <- readFile wordlistfile
  return $ map (map toLower) $ filter nobadchars $ lines ws

isword :: [String] -> String -> Bool
isword = undefined

chooseword :: WordPlayer -> String -> [String] -> IO (String)
chooseword IOPlayer ls ws = return $ ws !! 1
chooseword AIPlayer ls ws = return $ ws !! 2
  
playround :: Int -> [String] -> WordPlayer -> String -> WordPlayer -> String -> IO (Int, Int)
playround r ws p1 p1l p2 p2l = do
  print $ "Round " ++ show r
  w1 <- chooseword p1 p1l ws
  w2 <- chooseword p2 p2l ws
  case smashword w1 w2 of
    (p1, p2, LeftSide) -> do
      print $ "Player 1 won: " ++ (show . length $ p1) ++ " points"
      return (length p1, 0)
    (p1, p2, RightSide) -> do
      print $ "Player 2 won: " ++ (show . length $ p2) ++ " points"
      return (0, length p2)
    _ -> do
      print "Tie"
      return (0, 0)

playgame :: [String] -> Int -> WordPlayer -> WordPlayer -> IO ()
playgame ws r p1 p2 = do
  scores <- mapM (\r -> playround r ws p1 [] p2 []) [1..r]
  let p1s = sum $ map fst scores
  let p2s = sum $ map snd scores
  print $ "Player 1 score: " ++ show p1s
  print $ "Player 2 score: " ++ show p2s
  case compare p1s p2s of
    EQ -> print "It's a tie!"
    LT -> print "Congratulations Player 2!"
    GT -> print "Congratulations Player 1!"
  return ()

main = do
  wordlist <- getwords
  playgame wordlist turns IOPlayer AIPlayer
  return ()
