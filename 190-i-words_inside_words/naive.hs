import qualified Data.Text as T

{- Wrote a naive solver, it's really inefficient -}

data CharCount = Node Char Int [CharCount] deriving (Read, Show)

addWord :: String -> [CharCount] -> [CharCount]
addWord [] x      = x
addWord (x:xs) [] = [Node x 1 (addWord xs [])]
addWord (x:xs) ns = remainder ++
  case existing of
    []              -> [Node x 1 (addWord xs [])]
    [(Node a b cs)] -> [Node a (b+1) (addWord xs cs)]
  where existing = filter (byChar x) ns
        remainder = filter (byNotChar x) ns

byChar :: Char -> CharCount -> Bool
byChar a (Node c i ns) = a == c
              
byNotChar :: Char -> CharCount -> Bool
byNotChar a (Node c i ns) = a /= c

naive :: [String] -> [(String, Int)]
naive ws = map (\w -> (w, countEm w ws)) ws

countEm :: String -> [String] -> Int
countEm w ws = foldr (\x -> \y -> y + if T.isInfixOf (T.pack x) (T.pack w) then 1 else 0) 0 ws

main = do
  i <- getContents
  print (naive $ words i)

{- Some rough working out:
-- hello
-- hell

-- hello
-- hell

hello -> hell
hello -> el
hell ->

  
-- H1 -> I1

-- (H:I:T:[]) [(H1[I1[]])] =
-- (I:T:[]) [(H2[I1[]])] =
-- (T:[]) [(H2[I2[]])] =
-- [] [(H2[I2[T1]])] = 

--
-- g r a y - s - o n
-- 

inits' xs = 
-}
