module WordCannon (Winner (..), smashword) where

import Data.List (delete)
import Data.Char (toLower)

data Winner = LeftSide | RightSide | Tie
                                     deriving Show

smashword :: String -> String -> (String, String, Winner)
smashword l r = smashword' l [] r
  where l' = map toLower $ l
        r' = map toLower $ r

smashword' :: String -> String -> String -> (String, String, Winner)
smashword' [] ls rs = (ls, rs, winner)
  where winner = case compare ls rs
                 of LT -> RightSide
                    GT -> LeftSide
                    EQ -> Tie
smashword' (a:as) ls rs = if remainder == rs
                          then smashword' as (a:ls) rs
                          else smashword' as ls remainder
  where remainder = delete a rs
