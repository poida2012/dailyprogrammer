(defun lights-in-series-row (total-volts item-volts)
  (floor (/ total-volts item-volts)))

(defun rows-in-parallel (total-capacity item-amps time)
  (floor (/ total-capacity (* time item-amps))))

(defun max-lights-grid-dimensions (led-volts led-amps battery-volts battery-capacity time)
  (let ((leds-in-series (lights-in-series-row battery-volts led-volts)))
    (let ((number-of-rows (rows-in-parallel battery-capacity led-amps time)))
     (values leds-in-series number-of-rows))))

(defun render-row (leds-in-series)
  (loop for i from 1 to leds-in-series do
       (format t (if (eq i 1) "-|>|-" "--|>|-"))))

(defun render-spacer (leds-in-series)
  (loop for i from 1 to leds-in-series do
       (format t (if (eq i 1) "     " "      "))))

(defun render-grid (leds-in-series number-of-rows)
  (let ((first-row t)
	(vertical-spacer nil))

    (do ((i 1 (+ i 1)))
	((> i (- (* 2 number-of-rows) 1)) nil)

      (setf first-row (eq i 1))
      (setf vertical-spacer (eq (mod i 2) 0))

      (format t (if first-row "*" " "))
      (format t (if (not vertical-spacer) "-" "|"))

      (if (not vertical-spacer)
	  (render-row leds-in-series)
	  (render-spacer leds-in-series))
      
      (format t (if (not vertical-spacer) "-" "|"))
      (format t (if first-row "*~%" "~%")))))

(defun calculate-resistor (led-volts leds-in-series battery-volts battery-capacity time)
  (let ((drop-volts (- battery-volts (* led-volts leds-in-series)))
	(current-amps (/ battery-capacity time 1000)))
    (/ drop-volts current-amps)))

(defun generate-schematic (led-volts led-amps battery-volts battery-capacity time)
  (multiple-value-bind (leds-in-series number-of-rows) (max-lights-grid-dimensions led-volts led-amps battery-volts battery-capacity time)
    (let ((resistance (calculate-resistor led-volts leds-in-series battery-volts battery-capacity time)))
      
      (format t "Resistor: ~d Ohms~%" resistance)
      
      (format t "Scheme:~%")
      (render-grid leds-in-series number-of-rows))))
