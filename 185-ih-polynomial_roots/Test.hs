{-# LANGUAGE OverloadedStrings #-}

import Graphics.Vty.Widgets.All
import Graphics.Vty.Attributes
import qualified Data.Text as T

{- Test file to try out Vty as a terminal emulator -}

main :: IO ()
main = do
  f <- editWidget
  e <- plainText "hello world" >>= withNormalAttribute (fgColor blue)

  e `onKeyPressed` \this key mods -> do
    shutdownUi
    return True

  fg <- newFocusGroup

  c <- newCollection

  _ <- addToFocusGroup fg e
  _ <- addToCollection c e fg

  runUi c defaultContext
